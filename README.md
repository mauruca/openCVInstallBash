# openCVInstallBash
Bash script to install latest version or a specified version of OpenCV. <br>

For OpenCV license please read https://github.com/Itseez/opencv/blob/master/LICENSE.
