#!/bin/bash

version=$1

if [ -z $version ]
then
  echo "Use [<script.sh> last] to get the latest version."
  echo "Use [<script.sh> list] to list all versions available."
  echo "Use [<script.sh> 3.0.0] to install a specific version."
  exit 0
fi

if [ $version == "list" ]
then
  echo "OpenCV versions available..."
  echo "$(wget -q -O - http://sourceforge.net/projects/opencvlibrary/files/opencv-unix | egrep -o '\"[0-9](\.[0-9]+)+' | cut -c2-)"
  exit 0
fi

if [ $version == "last" ]
then
  version="$(wget -q -O - http://sourceforge.net/projects/opencvlibrary/files/opencv-unix | egrep -m1 -o '\"[0-9](\.[0-9]+)+' | cut -c2-)"
fi

echo "Removing any pre-installed ffmpeg and x264"
sudo apt-get -qq remove ffmpeg x264 libx264-dev
echo "Installing Dependenices"
sudo apt-get -qq install libopencv-dev build-essential checkinstall cmake pkg-config yasm libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev python-dev py$

echo "Installing OpenCV" $version
if [ ! -d ./OpenCV ]
then
  mkdir OpenCV
fi
cd OpenCV

if [ ! -f ./OpenCV-"$version".zip ]
then
  echo "Downloading OpenCV" $version
  wget -O OpenCV-$version.zip http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/$version/opencv-"$version".zip/download
fi

echo "Installing OpenCV" $version
unzip OpenCV-$version.zip

cd opencv-$version
if [ ! -d ./build ]
then
  mkdir build
fi

cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=OFF -D WITH_OPENGL=ON ..
make -j2
sudo checkinstall
sudo sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig

echo "OpenCV" $version "ready to be used"

